#!/bin/bash

#####################################
#   festivals by number of visits   #
#####################################

# write the uniq names of all festivals to file festivals
cat input | grep 'f,' | cut -d ',' -f3 | sort -r | uniq > festivals
rm tmp

# while loop which goes through every line of file festivals
cat festivals | while read line
do
    # calculate occurence of each festival from input
    cat input | grep 'f,' | grep "$line" | cut -d ',' -f3 | sort -r | uniq -c | tr -d "\n" >> tmp
    echo -n ' ( ' >> tmp

    # grep for each festivals date and display without newline
    tac input | grep "$line" | cut -d ',' -f2 | tr '\n' ' ' >> tmp
    echo ')' >> tmp
done

# print header
rm all_festivals.md
echo '```' >> all_festivals.md
echo 'Count   Festivals' >> all_festivals.md
cat tmp | sort -r >> all_festivals.md


####################################
#   concerts by number of visits   #
####################################

# write the uniq names of all concerts to file concerts
cat input | grep 'c,' | cut -d ',' -f3 | sort | uniq > concerts
rm tmp

# while loop which goes through every line of file concerts
cat concerts | while read line
do
    # calculate occurence of each festival from input
    cat input | grep 'c,' | grep "$line" | cut -d ',' -f3 | sort -r | uniq -c | tr -d "\n" >> tmp
    echo -n ' ( ' >> tmp

    # grep for each concerts date and display without newline
    tac input | grep "$line" | cut -d ',' -f2 | tr '\n' ' ' >> tmp
    echo ')' >> tmp
done

# print header
rm all_concerts.md
echo '```' > all_concerts.md
echo 'Count   Concerts' >> all_concerts.md
cat tmp | sort -r >> all_concerts.md


#############################
#   concerts by last seen   #
#############################

# print header
rm last_seen.md
echo '```' > last_seen.md
echo 'Count   Concerts' >> last_seen.md

# write the uniq names of all concerts to file last_seen.md
cat input | grep 'c,' | cut -d ',' -f2,3 | tr ',' '\t' | cat >> last_seen.md
