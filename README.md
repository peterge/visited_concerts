The [input](input) file contains the data with all concerts/festivals I have already visited.

Three markdown lists exist, and are created by running [create_md.sh](create_md.sh) from the date in [input](input):

- [All concerts, sorted by the number of times I have seen each band.](all_concerts.md)
- [All festivals, sorted by the number of times I have been there.](all_festivals.md)
- [A list starting with the last concert I saw live.](last_seen.md)

The current data is structured in this order:

```
---
a,2018,Architects
a,2018,BMTH
f,2018,Rock am Ring
---
a,2019,Archtitects
f,2019,Rock am Ring

f,          festivals
---         all concerts between f, and --- were acts during that festival
c,          concerts
 ,20$$,     year i went to festival/saw band X
      ,*    name of the band/festival I visited
```

I dont really know why I came to this idea, but quite often a converation with friends end like this:

"Oh yeah, I saw already saw band X on stage quite a lot of times!"

"Me too, how often?"

"Let me count: 1, 2, 3, ähhmm, I swear it was more than three times, but I dont remember how often..."


To solve this project, this idea of this fun project was born and during a boring sunday before my next festival (RaR 2022), this idea brought to life.

Also see [this](https://www.reddit.com/r/BattleJackets/comments/ux03jl/first_time_i_present_my_finished_jacket_which_i/) Reddit post, which shows the state of my battle jacket in 2022!